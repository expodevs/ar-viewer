<? if( ! defined('BASEPATH'))exit('No direct script access allowed');

class mailer{
	
	function send($textfrom, $to, $subject, $body, $rassilka=false, $files=array()){
		$EOL = "\r\n";
		
		$HTMLmessage = $this->return_html_version_email($subject, $body);
		$headfrom = "=?utf-8?b?".base64_encode($textfrom)."?= <".DEFAULT_EMAIL.">";
		//$headReplyTo = "=?utf-8?b?".base64_encode($textfrom)."?= <".EMAIL_FOR_COPY.">";
		$headReplyTo = "=?utf-8?b?".base64_encode($textfrom)."?= <".DEFAULT_EMAIL.">";
		$headsubject = "=?utf-8?b?".base64_encode($subject)."?=";
		
		if($rassilka){
			$headpart = "List-Unsubscribe: ".BASEURL."account/send_price_info/".$EOL; //"Precedence: bulk".$EOL;
			$body .= $EOL.$EOL."Для того чтобы отписаться от рассылки перейдите по ссылке: ".BASEURL."account/send_price_info/";
			$HTMLmessage .= "<br><br>Для того чтобы отписаться от рассылки перейдите по ссылке: <a href='".BASEURL."account/send_price_info/'>Отписаться</a>";
		}else{ $headpart = ""; }
		
		$boundary1 = md5(uniqid(time()));
		$boundary2 = md5(uniqid(time()));
		
		if(!empty($files)){
			$headers =
				"Content-Type: multipart/mixed; boundary=\"$boundary1\"".$EOL.
				"MIME-Version: 1.0".$EOL.
				"From: $headfrom".$EOL.
				"To: $to".$EOL.
				"Reply-To: $headReplyTo".$EOL.
				"Errors-To: ".DEFAULT_EMAIL.$EOL.
				"X-Complaints-To: ".DEFAULT_EMAIL.$EOL.
				"X-Sender: ".DEFAULT_EMAIL.$EOL.
				"Subject: $headsubject".$EOL.
				$headpart.
				$EOL."--$boundary1".$EOL.
				"Content-type: multipart/alternative; boundary=\"$boundary2\"".$EOL.
				"MIME-Version: 1.0".$EOL;
		}else{
			$headers = 
				"Content-type: multipart/alternative; boundary=\"$boundary2\"".$EOL.
				"MIME-Version: 1.0".$EOL.
				"From: $headfrom".$EOL.
				"To: $to".$EOL.
				"Reply-To: $headReplyTo".$EOL.
				"Errors-To: ".DEFAULT_EMAIL.$EOL.
				"X-Complaints-To: ".DEFAULT_EMAIL.$EOL.
				"X-Sender: ".DEFAULT_EMAIL.$EOL.
				"Subject: $headsubject".$EOL.
				$headpart;
		}
		
		$headers .=
			$EOL."--$boundary2".$EOL.
			"MIME-Version: 1.0".$EOL.
			"Content-Type: text/plain; charset=utf-8".$EOL.
			"Content-Transfer-Encoding: base64".$EOL.$EOL.
			chunk_split(base64_encode($body)).
			"--$boundary2".$EOL.
			"MIME-Version: 1.0".$EOL.
			"Content-Type: text/html; charset=utf-8".$EOL.
			"Content-Transfer-Encoding: base64".$EOL.$EOL.
			chunk_split(base64_encode($HTMLmessage)).
			"--$boundary2--".$EOL;
		if(!empty($files)){
			foreach($files as $file){
				$fp = fopen($file['patch'],"rb");
				$file2 = fread($fp, filesize($file['patch']));
				fclose($fp);
				
				$headers .=
					"--$boundary1".$EOL.
					"MIME-Version: 1.0".$EOL.
					"Content-Type: application/octet-stream; name=\"".$file['basename']."\"".$EOL.
					"Content-Transfer-Encoding: base64".$EOL.
					"Content-Disposition: attachment; filename=\"".$file['basename']."\"".$EOL.$EOL.
					chunk_split(base64_encode($file2)).$EOL;
			}
			$headers .= "--$boundary1--";
		}
		
		//pre($headers);exit;
		
        $output = "";
		ini_set("SMTP", "localhost");
		if($smtp_conn = fsockopen("localhost", 25, $errno, $errstr, 30)){
			if($errno > 0){
				echo $errstr;
			}else{
				fputs($smtp_conn, "HELO ".DOMEN.$EOL);						$output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
				//fputs($smtp_conn, "AUTH NONE".$EOL);						$output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
                
                fputs($smtp_conn, "AUTH LOGIN\r\n");						$output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
                //fputs($smtp_conn, 'STARTTLS'.$EOL);                       $output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
                fputs($smtp_conn, base64_encode(DEFAULT_EMAIL)."\r\n");     $output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
                fputs($smtp_conn, base64_encode("CWmzkUEsWK")."\r\n");      $output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
                
				fputs($smtp_conn, "MAIL FROM: ".DEFAULT_EMAIL.$EOL);		$output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
				fputs($smtp_conn, "RCPT TO: $to".$EOL);						$output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
				fputs($smtp_conn, "DATA".$EOL);								$output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
				fputs($smtp_conn, $headers.$EOL.".".$EOL);                  $output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
				fputs($smtp_conn, "QUIT".$EOL);								$output .= "<span style='display:none;'>".fgets($smtp_conn)."<br></span>";
				fclose($smtp_conn);
			}
		}
		
		//mail($to, $subject, $message, $headers);
	}
	
	function return_html_version_email($subject, $body){
		return "<!DOCTYPE html>
			<html lang='ru-RU'>
				<head>
					<meta charset='utf-8'>
					<meta name='viewport' content='width=device-width'>
					<title>$subject</title>
				</head>
				<body>$body</body>
			</html>";
	}
	
	
}