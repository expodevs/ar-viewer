<? if( ! defined('BASEPATH'))exit('No direct script access allowed');

class crud{
	
	public $prefix;
	public $connection;
    public $check;
    
    protected static $_instance;
    
    public static function getInstance() { // получить экземпляр данного класса
        if (self::$_instance === null) { // если экземпляр данного класса  не создан
            self::$_instance = new self;  // создаем экземпляр данного класса
        }
        return self::$_instance; // возвращаем экземпляр данного класса
    }
	
	private function __construct(){
		$this->prefix = DBprefix;
		$this->connection = mysqli_connect(DBhost, DBuser, DBpassword, DBbase)or die("MySQL сервер недоступен, или Нет соединения с БД. ");
        mysqli_query($this->connection, "SET NAMES 'utf8mb4'");
		mysqli_query($this->connection, "SET CHARACTER SET 'utf8mb4'");
		mysqli_query($this->connection, "SET SESSION collation_connection = 'utf8mb4_unicode_ci'");
        mysqli_set_charset($this->connection, 'utf8mb4');
        
        $this->check = true;
	}
	
    private function checkdata($data){
        if($this->check){
            //Если передан массив
            if(gettype($data)=="array"){
                foreach($data as $value){
                    if(gettype($value)=="string"){
                        if(strripos($value, "DELETE ") !== false){ exit; }
                        if(strripos($value, "UPDATE ") !== false){ exit; }
                        if(strripos($value, "INSERT ") !== false){ exit; }
                        if(strripos($value, "TRUNCATE ") !== false){ exit; }
                        if(strripos($value, "DROP ") !== false){ exit; }
                        if(strripos($value, "ALTER ") !== false){ exit; }
                        if(strripos($value, "PRIVILEGES ") !== false){ exit; }
                        if(strripos($value, "GRANT ") !== false){ exit; }
                    }
                }
            }
            //Если передан текст
            else{
                if(strripos($data, "DELETE ") !== false){ exit; }
                if(strripos($data, "UPDATE ") !== false){ exit; }
                if(strripos($data, "INSERT ") !== false){ exit; }
                if(strripos($data, "TRUNCATE ") !== false){ exit; }
                if(strripos($data, "DROP ") !== false){ exit; }
                if(strripos($data, "ALTER ") !== false){ exit; }
                if(strripos($data, "PRIVILEGES ") !== false){ exit; }
                if(strripos($data, "GRANT ") !== false){ exit; }
            }
        }
    }
    
	function create($table, $data){
        $this->checkdata($data);
        
		$values=""; $sep="";
		foreach($data as $key => $val){
            //Если значение передано булево
            if(gettype($val)=="boolean"){
                if($val==true){ $values .= $sep.$key."=true"; }
                else{ $values .= $sep.$key."=false"; }
            }
            //Если передана строка
            elseif(gettype($val)=="string"){
                $values .= $sep.$key."='".trim(mysqli_real_escape_string($this->connection, $val))."'";
            }
            //Если передано NULL
            elseif(gettype($val)=="NULL"){
                $values .= $sep.$key."=NULL";
            }
            //Все остальное
            else{
                $values .= $sep.$key."='".trim($val)."'";
            }
			$sep = ", ";
		}
        
		$query = "INSERT INTO ".$this->prefix."$table SET $values";
		mysqli_query($this->connection, $query)or die('Ошибка создания записи: '.mysqli_error($this->connection));
		return mysqli_insert_id($this->connection);
	}
	
	function read($table, $condition = array(), $all_records=false, $columns="*", $returnobject=true, $orderby="", $keyCol=""){
        $this->checkdata($condition);
        
        //Передан массив
		if(gettype($condition)=="array"){
			if(count($condition)>0){ $where = "WHERE "; }else{ $where = ""; }
			$sep="";
			foreach($condition as $key => $value){
                //Если значение передано булево
                if(gettype($value)=="boolean"){
                    if($value==true){$where .= $sep.$key."=true";}
                    else{$where .= $sep.$key."=false";}
                }
                //Если передано NULL
                elseif(gettype($value)=="NULL"){
                    $where .= $sep.$key."=NULL";
                }
                //Если передана строка
                elseif(gettype($value)=="string"){
                    $where .= $sep.$key."='".trim(mysqli_real_escape_string($this->connection, $value))."'";
                }
                //Все остальное
                else{
                    $where .= $sep.$key."='".trim($value)."'";
                }
                $sep = " AND ";
			}
		}
        //Передана строка
        elseif(gettype($condition)=="string"){
			$where = "WHERE ".$condition;
		}
        
        $query = "SELECT $columns FROM ".$this->prefix."$table $where $orderby ".($all_records ? "" : "LIMIT 1");
        $result = mysqli_query($this->connection, $query)or die('Ошибка чтения: '.mysqli_error($this->connection));
        
        //Если пустой результат запроса
		if(mysqli_num_rows($result)==0){  return 0;  }
        //Если все записи
        elseif($all_records==true){
			$arr = array();
			if($returnobject){
                while($res = mysqli_fetch_object($result)){
                    if(!empty($keyCol)){
                        $arr[$res->$keyCol] = $res;
                    }else{
                        $arr[] = $res;
                    }
                }
            }
            else{
                while($res = mysqli_fetch_assoc($result)){
                    if(!empty($keyCol)){
                        $arr[$res->$keyCol] = $res;
                    }else{
                        $arr[] = $res;
                    }
                }
            }
            mysqli_free_result($result);
			return $arr;
		}
        //Если одна запись
        else{
			if($returnobject){ $res = mysqli_fetch_object($result); }
			else{ $res = mysqli_fetch_assoc($result); }
            mysqli_free_result($result);
			return $res;
		}
	}
	
	function update($table, $data, $where){
        $this->checkdata($data);
        
        //Если передан массив
		if(gettype($data)=="array"){
            $values=""; $sep="";
            foreach($data as $key => $val){
                //Если значение передано булево
                if(gettype($val)=="boolean"){
                    if($val==true){ $values = $values.$sep.$key."=true"; }
                    else{ $values .= $sep.$key."=false"; }
                }
                //Если передано NULL
                elseif(gettype($val)=="NULL"){
                    $values .= $sep.$key."=NULL";
                }
                //Если передана строка
                elseif(gettype($val)=="string"){
                    $values .= $sep.$key."='".trim(mysqli_real_escape_string($this->connection, $val))."'";
                }
                //Все остальное
                else{
                    $values .= $sep.$key."='".trim($val)."'";
                }
                $sep = ", ";
            }
        }
        //Если передан текст
        else{
            $values = $data;
        }
        
        
        //Если передан массив
        if(gettype($where)=="array"){
            $wh=""; $sep="";
            foreach($where as $key => $value){
                //Если значение передано булево
                if(gettype($value)=="boolean"){
                    if($value==true){$wh = $wh.$sep.$key."=true";}
                    else{$wh = $wh.$sep.$key."=false";}
                }
                //Если передано NULL
                elseif(gettype($value)=="NULL"){
                    $wh .= $sep.$key."=NULL";
                }
                //Если передана строка
                elseif(gettype($value)=="string"){
                    $wh .= $sep.$key."='".trim(mysqli_real_escape_string($this->connection, $value))."'";
                }
                //Все остальное
                else{
                    $wh = $wh.$sep.$key."='".trim($value)."'";
                }
                $sep = " AND ";
            }
        }
        //Если передан текст
        else{
            $wh = $where;
        }
        
        
		$query = "UPDATE ".$this->prefix.$table." SET $values WHERE $wh";
		mysqli_query($this->connection, $query)or die('Ошибка обновления: '.mysqli_error($this->connection));
	}
	
	function delete($table, $condition){
        $this->checkdata($condition);
        
		$where=""; $sep="";
        //Передан массив
        if(gettype($condition)=="array"){
            foreach($condition as $key => $value){
                if(gettype($value)=="boolean"){ //Если значение передано булево
                    if($value==true){ $where .= $sep.$key."=true"; }
                    else{ $where .= $sep.$key."=false"; }
                }else{
                    $where .= $sep.$key."='".trim($value)."'";
                }
                $sep = " AND ";
            }
        }
        //Передана строка
        else{
            $where = $condition;
        }
        
		$query = "DELETE FROM ".$this->prefix."$table WHERE $where";
		mysqli_query($this->connection, $query)or die('Ошибка удаления: '.mysqli_error($this->connection));
	}
	
	function query($query){
		$result = mysqli_query($this->connection, $query)or die('Ошибка запроса: '.mysqli_error($this->connection));
		return $result;
	}
    
    function customquery($query){
        return $this->query($query);
    }
    
    function numrows($table_name, $condition=""){
        
		//Условие пусто
        if(empty($condition)){
            $where = "";
        }
        //Передан массив
		elseif(gettype($condition)=="array"){
			if(count($condition)>0){ $where = "WHERE "; }else{ $where = ""; }
			$sep="";
			foreach($condition as $key => $value){
                //Если значение передано булево
                if(gettype($value)=="boolean"){
                    if($value==true){$where .= $sep.$key."=true";}
                    else{$where .= $sep.$key."=false";}
                }
                //Если передано NULL
                elseif(gettype($value)=="NULL"){
                    $where .= $sep.$key."=NULL";
                }
                //Если передана строка
                elseif(gettype($value)=="string"){
                    $where .= $sep.$key."='".trim(mysqli_real_escape_string($this->connection, $value))."'";
                }
                //Все остальное
                else{
                    $where .= $sep.$key."='".trim($value)."'";
                }
                $sep = " AND ";
			}
		}
        //Передана строка
        elseif(gettype($condition)=="string"){
			$where = "WHERE ".$condition;
		}
        
        
        $result = mysqli_query($this->connection, "SELECT COUNT(1) FROM $this->prefix$table_name $where")or die('Ошибка в numrows: '.mysqli_error($this->connection));
        $cnt = mysqli_fetch_array($result);
        
        mysqli_free_result($result);
        return $cnt[0];
    }
	
    function query_in_object($query){
        //Если результат запроса пустой
        if(mysqli_num_rows($query)==0){
            mysqli_free_result($query);
            return 0;
        }
        //Не пустой
        else{
            $result = array();
            while($res = mysqli_fetch_object($query)){ $result[] = $res; }
            
            mysqli_free_result($query);
            return $result;
        }
    }
    
    function close(){
        mysqli_close($this->connection);
    }
}