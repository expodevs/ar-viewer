<? if( ! defined('BASEPATH'))exit('No direct script access allowed');

class index extends kernel{
	
	function __construct($param){
		parent::__construct();
	}
	
    function index(){

        $this->view('common/header');
        $this->view('index');
        $this->view('common/footer');
	}

    function ajax($param){
        if(empty($param[1])){  get_error(); exit;  }
        elseif($param[1]=="get_app"){
            if(empty($_POST['id'])){ exit; }
            $data = $this->crud->read('b_apps', array('id'=>$_POST['id']));
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
        }
    }

	function apps($param) {

        $this->options->apps = $this->crud->read('b_apps', ['user_id' => $this->options->user->id], true, "*", true, "ORDER BY id DESC");

        //Добавление
        if($param[1]=="add"){

            if(empty($_POST['name_app'])){  header("Location: ".BASEURL."apps/"); exit;  }
            $dataToInsert = array();

            $dataToInsert['name'] = $_POST['name_app'];
            
            $dataToInsert['user_id'] = $_POST['user_id'];

            //Загрузка файла
            $nameMD5 = md5($_POST['name_app']);

            if(!empty($_POST['obj_title_app'])) {
                $dataToInsert['title_object'] = $_POST['obj_title_app'];
            }
            $dataToInsert['date'] = time();
            if(!empty($_POST['marker_app'])){
                /*$ext1 = pathinfo($_FILES['marker_app']['name'], PATHINFO_EXTENSION);
                $new_file1 = $nameMD5.'.'.$ext1;
                $new_file_with_path1 = $_SERVER['DOCUMENT_ROOT'].'/data/markers/'.$new_file1;
                copy($_FILES['marker_app']['tmp_name'], $new_file_with_path1);
                $dataToInsert['marker_url'] = $new_file1;*/
                $urlMarker = $_SERVER['DOCUMENT_ROOT'].'data/markers/'.$nameMD5 . '.patt';
                file_put_contents($urlMarker, $_POST['marker_app']);
                $dataToInsert['marker_url'] = $nameMD5 . '.patt';
            }
            if(!empty($_POST['marker_img_app'])){
                $urlImgMarker = $_SERVER['DOCUMENT_ROOT'].'data/markers/'.$nameMD5 . '.png';
                file_put_contents($urlImgMarker, base64_decode($_POST['marker_img_app']));
                $dataToInsert['marker_img_url'] = $nameMD5 . '.png';
            }
            if(!empty($_FILES['obj_app'])){
                $ext3 = pathinfo($_FILES['obj_app']['name'], PATHINFO_EXTENSION);
                $new_file3 = $nameMD5.'.'.$ext3;

                if($ext3 == 'png' || $ext3 == 'jpg') {
                    $new_file_with_path3 = $_SERVER['DOCUMENT_ROOT'].'/data/image/'.$new_file3;
                    $dataToInsert['object_url'] = 'image/'. $new_file3;
                    $dataToInsert['type_object'] = 1;
                } elseif ($ext3 == 'mp4') {
                    $new_file_with_path3 = $_SERVER['DOCUMENT_ROOT'].'/data/video/'.$new_file3;
                    $dataToInsert['object_url'] = 'video/' . $new_file3;
                    $dataToInsert['type_object'] = 2;
                } elseif ($ext3 == 'gltf') {
                    $new_file_with_path3 = $_SERVER['DOCUMENT_ROOT'].'/data/3d-models/'.$new_file3;
                    $dataToInsert['object_url'] = '3d-models/' . $new_file3;
                    $dataToInsert['type_object'] = 3;
                } elseif ($ext3 == 'mp3') {
                    $new_file_with_path3 = $_SERVER['DOCUMENT_ROOT'].'/data/audio/'.$new_file3;
                    $dataToInsert['object_url'] = 'audio/' . $new_file3;
                    $dataToInsert['type_object'] = 4;
                } else {
                    header("Location: ".BASEURL."apps/"); exit;
                }

                copy($_FILES['obj_app']['tmp_name'], $new_file_with_path3);
            }


            include_once('vendor/phpqrcode/qrlib.php');
            QRcode::png(BASEURL.'app/'.$nameMD5.'/', $_SERVER['DOCUMENT_ROOT'] . '/data/qr-codes/'.$nameMD5.'.png');

            $dataToInsert['link'] = $nameMD5;
            $dataToInsert['qr_code'] = $nameMD5.'.png';



            $this->crud->create('b_apps', $dataToInsert);
            header("Location: ".BASEURL."apps/"); exit;
        }

        //Удаление
        elseif($param[1]=="delete" && !empty($param[2])){

            $app = $this->crud->read('b_apps', ['id'=>$param[2]]);
            $appStr = $app->link;
            $appType = $app->type_object;

            try{ unlink($_SERVER['DOCUMENT_ROOT'].'/data/'.$app->object_url); }catch(Throwable $e){ }
            try{ unlink($_SERVER['DOCUMENT_ROOT'].'/data/markers/'.$app->marker_img_url); }catch(Throwable $e){ }
            try{ unlink($_SERVER['DOCUMENT_ROOT'].'/data/markers/'.$app->marker_url); }catch(Throwable $e){ }
            try{ unlink($_SERVER['DOCUMENT_ROOT'].'/data/qr-codes/'.$app->qr_code); }catch(Throwable $e){ }

            $this->crud->delete('b_apps', array('id'=>$param[2]));
            header("Location: ".BASEURL."apps/"); exit;
        }

        $this->view('common/header');
        $this->view('apps');
        $this->view('common/footer');
    }

    function test(){        
        $this->view('common/header');
		$this->view('page_test');
		$this->view('common/footer');
    }

    function error_404(){
        header("HTTP/1.0 404 Not Found");
        
        $this->view('common/header');
		$this->view('error_404');
		$this->view('common/footer');
    }

    function login(){
        //Залогиненым тут делать нечего
        if($this->options->logedinadmin){  header("Location: ".BASEURL."admin/");  exit;  }
        elseif($this->options->logedin){   header("Location: ".BASEURL);           exit;  }

        //Логинемся
        if(!empty($_POST['email']) && !empty($_POST['password'])){
            $pass = md5($_POST['password']);
            $user = $this->crud->read('b_users', array('email'=>$_POST['email'], 'password'=>$pass));

            if(empty($user)){  header("Refresh: 3; Url=".$_SERVER['HTTP_REFERER']);  echo "Электронный адрес или пароль не верны!";  exit;  }

            setcookie('email', $_POST['email'], time()+60*60*24*365, '/', DOMEN);
            setcookie('password', $pass, time()+60*60*24*365, '/', DOMEN);

            if($user->admin){  header("Location: ".BASEURL."admin/");  exit;  }
            header("Location: ".BASEURL);  exit;
        } else {
            $this->view('common/header');
            $this->view('login');
            $this->view('common/footer');
        }
    }

    function register(){
        //Залогиненным тут делать нечего
        if($this->options->logedin){ header("Location: ".BASEURL); exit; }
        if(!empty($_POST['name']) && !empty($_POST['phone']) && !empty($_POST['email']) && !empty($_POST['password'])){
            $email = htmlspecialchars(trim(strip_tags($_POST['email'])));
            $password = md5($_POST['password']);
            $re_password = md5($_POST['re_password']);
            //Пароли не совпадают
            if($password != $re_password){
                header("Refresh: 3; Url=".$_SERVER['HTTP_REFERER']);
                ?>
                <div style="position:relative; width:100%; height:100%;">
                    <div style="position:absolute; width:300px; height:150px; margin:auto; top:0; bottom:0; left:0; right:0; text-align:center;">
                        <h3>Пароли не совпадают!</h3>
                    </div>
                </div>
                <?
                exit;
            }
            //Заносим учетку в базу
            else{
                $dataToInsert = array();
                $dataToInsert['email'] = $email;
                $dataToInsert['password'] = $password;
                $dataToInsert['fio'] = htmlspecialchars(trim(strip_tags($_POST['name'])));
                $dataToInsert['phone'] = str_replace(array("|", "-", " ", ")", "("), "", htmlspecialchars(trim(strip_tags($_POST['phone']))));
                $this->crud->create('b_users', $dataToInsert);
                setcookie('email', $email, time()+60*60*24*7, '/', DOMEN);
                setcookie('password', $password, time()+60*60*24*7, '/', DOMEN);
                if(isset($_SERVER['HTTP_REFERER'])){
                    header('Location: '.$_SERVER['HTTP_REFERER']);
                }else{
                    header('Location: '.BASEURL);
                }
                exit;
            }
        }
        $this->view('common/header');
        $this->view('register');
        $this->view('common/footer');

    }

    function signout(){
        setcookie('email', "", time()-3600, "/", DOMEN);
        setcookie('password', "", time()-3600, "/", DOMEN);
        header("Location: ".BASEURL); exit;
    }

    function marker_generate($param){
        $this->view('page_generate_marker');
    }

    function app($param) {
	    if(!empty($param[1])) {
	        $this->options->app = $this->crud->read('b_apps', ['link' => $param[1]]);

	        if($this->options->app->type_object == 1) {$this->view('page_image');}
	        elseif ($this->options->app->type_object == 2) {$this->view('page_video');}
	        elseif ($this->options->app->type_object == 3) {$this->view('page_model');}
	        elseif ($this->options->app->type_object == 4) {$this->view('page_audio');}

        }
    }

    function dynamic_method() {

    }

    
}//header("Refresh: 2; Url=".BASEURL);
?>