<!DOCTYPE html>
<title>AR.js Marker Training</title>
<!-- include mdl -->
<link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'>
<link rel='stylesheet' href='https://code.getmdl.io/1.3.0/material.indigo-pink.min.css'>
<script defer src='https://code.getmdl.io/1.3.0/material.min.js'></script>
<link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>

<!-- Include pdfMake - http://pdfmake.org/ -->
<script src='<?=BASEURL?>vendor/pdfMake/pdfmake.min.js'></script>
<script src='<?=BASEURL?>vendor/pdfMake/vfs_fonts.js'></script>

<!-- include THREEx.ArPatternFile -->
<script src='<?=BASEURL?>vendor/threex-arpatternfile.js'></script>
<link rel="stylesheet" href="/css/main.css">

<style>
    .wrapper_lazy_load {
        opacity: 0;
        transition: .3s;
        min-height: calc(100vh - 131px);
    }
</style>
<style media='screen'>
    h1 {
        text-align: center;
        font-size: 48px;
    }

    #imageContainer img {
        width: 100%;
        max-width: 512px;
    }
    .mess-info {
        font-style: italic;
        font-size: 12px;
        margin-top: 7px;
    }
</style>
<body>
<header class="header">
    <div class="container">
        <div class="row justify_content_between align_items_center">
            <div class="shrink">
                <div class="logo">
                    <a href="<?=BASEURL?>">
                        <svg id="Capa_1" enable-background="new 0 0 512 512" height="60" viewBox="0 0 512 512"
                             width="60" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="256" x2="256" y1="512"
                                            y2="0">
                                <stop offset="0" stop-color="#ffe59a"/>
                                <stop offset="1" stop-color="#ffffd5"/>
                            </linearGradient>
                            <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="256" x2="256" y1="407"
                                            y2="105">
                                <stop offset="0" stop-color="#ffde00"/>
                                <stop offset="1" stop-color="#fd5900"/>
                            </linearGradient>
                            <g>
                                <g>
                                    <circle cx="256" cy="256" fill="url(#SVGID_1_)" r="256"/>
                                </g>
                                <g>
                                    <g>
                                        <path d="m193.124 255.997 12.573 12.573 12.582-12.573-12.582-12.573zm50.302 50.302 12.573 12.573 12.573-12.573-12.573-12.582zm155.805-45.387c-.009 0-.009 0-.009 0l-4.915-4.915 4.923-4.915c10.359-10.385 10.359-27.266-.009-37.651-10.081-10.073-27.717-10.125-37.877.052l-4.741 4.811-12.626-12.626 4.923-4.915c10.359-10.385 10.359-27.266-.009-37.66-10.385-10.351-27.266-10.359-37.651.009l-4.915 4.915-12.625-12.626 4.863-4.785c10.394-10.394 10.443-27.381 0-37.842-10.385-10.351-27.266-10.359-37.651.009l-4.915 4.915-4.915-4.923c-10.385-10.351-27.266-10.359-37.651.009-10.382 10.382-10.473 27.37.052 37.877l4.811 4.741-12.625 12.626-4.915-4.923c-10.385-10.351-27.257-10.359-37.66.009-10.359 10.385-10.359 27.266.009 37.651l4.915 4.915-12.626 12.626-4.784-4.863c-10.064-10.073-27.743-10.073-37.842 0-10.359 10.385-10.359 27.266.009 37.651l4.915 4.915-4.923 4.915c-10.359 10.385-10.359 27.266.009 37.651 10.081 10.081 27.726 10.116 37.877-.052l4.741-4.811 12.626 12.626-4.923 4.915c-10.359 10.385-10.359 27.266.009 37.66 10.385 10.351 27.266 10.359 37.651-.009l4.915-4.915 12.625 12.626-4.863 4.785c-5.036 5.036-7.806 11.749-7.806 18.921 0 7.164 2.77 13.876 7.806 18.921 10.385 10.351 27.266 10.359 37.651-.009l4.915-4.915 4.915 4.923c10.401 10.367 27.265 10.36 37.651-.009 10.382-10.382 10.473-27.37-.052-37.877l-4.811-4.741 12.625-12.626 4.915 4.923c10.385 10.351 27.257 10.359 37.659-.009 10.359-10.385 10.359-27.266-.009-37.651l-4.915-4.915 12.626-12.626 4.784 4.863c10.073 10.064 27.743 10.081 37.842 0 10.36-10.386 10.36-27.266.001-37.651zm-174.665-61.505 25.147-25.147c3.473-3.473 9.1-3.473 12.573 0l25.147 25.147c3.473 3.473 3.473 9.1 0 12.573l-25.147 25.156c-1.667 1.667-3.925 2.605-6.287 2.605s-4.62-.938-6.287-2.605l-25.147-25.156c-3.472-3.472-3.472-9.099.001-12.573zm-12.582 88.024c-1.737 1.737-4.012 2.605-6.287 2.605s-4.55-.868-6.287-2.605l-25.147-25.147c-3.473-3.473-3.473-9.1 0-12.573l25.147-25.147c3.473-3.473 9.1-3.473 12.573 0l25.156 25.147c1.667 1.667 2.605 3.925 2.605 6.287s-.938 4.62-2.605 6.287zm75.449 25.155-25.147 25.147c-1.737 1.737-4.012 2.605-6.287 2.605s-4.55-.868-6.287-2.605l-25.147-25.147c-3.473-3.473-3.473-9.1 0-12.573l25.147-25.156c3.334-3.334 9.239-3.334 12.573 0l25.147 25.156c3.474 3.473 3.474 9.1.001 12.573zm50.302-50.302-25.147 25.147c-1.737 1.737-4.012 2.605-6.287 2.605s-4.55-.868-6.287-2.605l-25.156-25.147c-1.667-1.667-2.605-3.925-2.605-6.287s.938-4.62 2.605-6.287l25.156-25.147c3.473-3.473 9.1-3.473 12.573 0l25.147 25.147c3.475 3.473 3.475 9.1.001 12.574zm-69.162-56.59-12.573-12.573-12.573 12.573 12.573 12.582zm25.147 50.303 12.582 12.573 12.573-12.573-12.573-12.573z"
                                              fill="url(#SVGID_2_)"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="shrink">
                <div class="title_project">Конструктор</div>
            </div>
            <div class="shrink">
                <div class="user_wrap">
                    <? if($this->logedin) {?>
                        <span class="name"><?=$this->user->fio?></span> <a href="<?=BASEURL?>signout/" class="btn btn_default">Выйти</a>
                    <? } else { ?>
                        <a href="<?=BASEURL?>login/" class="btn btn_default">Войти</a>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="wrapper_lazy_load">

    <div class="generate-page">
        <h1 class="mb30">Генерация маркера</h1>

        <div class="container">
            <div class="row">
                <div class="col_12 text_center mb20">
                    <label id='buttonUpload' for='fileinput' class='btn btn_default'>
                        <input type='file' id='fileinput' style='display: none'>
                        Загрузить
                    </label>
                </div>
            </div>
        </div>

        <div class="container mb20">
            <div class="row justify_content_center align_items_center">
                <div class="col_md_4">
                    <div id='imageContainer'></div>
                </div>
                <div class="shrink">
                    <div>
                        <button id='buttonDownloadEncoded' class='btn btn_default'>
                            Скачать маркер
                        </button>
                    </div>
                    <div class="mess-info mb20">Скачайте маркер</div>
                    <div>
                        <button id='buttonDownloadFullImage' class='btn btn_default'>
                            Скачать изображение
                        </button>
                    </div>
                    <div class="mess-info">Скачайте изображение. <br>Это изображение распечатайте на странице книги</div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer text_center">
    <div class="copy">Copyright 2021</div>
</footer>



<script>
    var innerImageURL = null;
    var fullMarkerURL = null;
    var imageName = null;

    innerImageURL = '/images/inner-arjs.png';
    updateFullMarkerImage();

    document.querySelector('#buttonDownloadEncoded').addEventListener('click', function(){
        if( innerImageURL === null ){
            alert('upload a file first');
            return
        }
        console.assert(innerImageURL);
        THREEx.ArPatternFile.encodeImageURL(innerImageURL, function onComplete(patternFileString){
            THREEx.ArPatternFile.triggerDownload(patternFileString, "pattern-" + (imageName || "marker") + ".patt")
        })
    });


    document.querySelector('#buttonDownloadFullImage').addEventListener('click', function(){
        // debugger
        if( innerImageURL === null ){
            alert('upload a file first');
            return
        }

        var domElement = window.document.createElement('a');
        domElement.href = fullMarkerURL;
        domElement.download = "pattern-" + (imageName || 'marker') + '.png';
        document.body.appendChild(domElement);
        domElement.click();
        document.body.removeChild(domElement)
    });



    document.querySelector('#fileinput').addEventListener('change', function(){
        var file = this.files[0];
        imageName = file.name;
        // remove file extension
        imageName = imageName.substring(0, imageName.lastIndexOf('.')) || imageName;

        // debugger

        var reader = new FileReader();
        reader.onload = function(event){
            innerImageURL = event.target.result;
            updateFullMarkerImage()
        };
        reader.readAsDataURL(file);
    });

    function updateFullMarkerImage(){
        // get patternRatio

        THREEx.ArPatternFile.buildFullMarker(innerImageURL, 0.9, 512, 'black', function onComplete(markerUrl){
            fullMarkerURL = markerUrl;

            var fullMarkerImage = document.createElement('img');
            fullMarkerImage.src = fullMarkerURL;

            // put fullMarkerImage into #imageContainer
            var container = document.querySelector('#imageContainer');
            while (container.firstChild) container.removeChild(container.firstChild);
            container.appendChild(fullMarkerImage)
        })
    }


</script>

<script src="/libs/jQuery/jquery-3-4-1.min.js"></script>
<script src="/js/main.js"></script>

<script>
    $(document).ready(function(){
        setTimeout(function () {
            $('.wrapper_lazy_load').css('opacity', 1);
        },100);
    })
</script>

</body>