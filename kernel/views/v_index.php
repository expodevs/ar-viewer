<div class="container">
    <div class="row justify_content_center mt50">
        <? if($this->logedin) {?>
            <div class="shrink"><a href="<?=BASEURL?>apps/" class="btn btn_success">Создать приложение</a></div>
        <? } ?>
    </div>
    <div class="row justify_content_center">
        <div class="col_md_7 my50">

            <div class="title_apps_con">Приветствуем Вас, <?=$this->user->fio?></div>
            <p>Это сайт конструктор веб-приложения с дополненной реальностью для книг! Здесь Вы можете загрузить для своей книги картинку, видео, аудио или 3d объект</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut dolorem expedita explicabo facere hic itaque molestias nisi quis totam veniam.</p>
        </div>
    </div>
</div>