<html>
<head>
<script src="/js/aframe.min.js"></script>
<script src="/js/aframe-ar.js"></script>
    <!-- <script src="aframe-ar.js"></script> use this (v1.6) if this demo ever breaks-->
    <link rel="stylesheet" href="/css/main.css">
    <style>
        .btns_control {
            position: absolute;
            bottom: 20px;
            left: 0;
            width: 100%;
            font-size: 18px;
            z-index: 9;
        }
        .btns_control a {
            width: 100%;
            text-align: center;
        }
    </style>
</head>
<body>

<a-scene>
  <a-assets>
    <img id="sky" src="/data-test/minon.jpg">
  </a-assets>
  <a-sky src="#sky"></a-sky>
  <a-marker-camera type="pattern" url="/data-test/marker.patt">
    <a-sky src="#sky"></a-sky>
    </a-marker-camera>
</a-scene>



</body>
</html>