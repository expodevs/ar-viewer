<!doctype HTML>
<html>
<script src="/js/aframe.min.js"></script>
<script src="/js/aframe-ar.js"></script>
<body style="margin: 0px; overflow: hidden;">

<a-scene embedded vr-mode-ui="enabled: false;" arjs="debugUIEnabled: false;">


    <a-marker type="pattern" url="/data/markers/<?=$this->app->marker_url?>">
        <a-entity   position="0 0 0"
                    rotation="0 0 0"
                    scale="0.7 0.7 0.7"
                    gltf-model="url(/data/<?=$this->app->object_url?>)" >
        </a-entity>
    </a-marker>

    <a-entity camera></a-entity>

</a-scene>
</body>
</html>