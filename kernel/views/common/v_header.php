<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Конструктор</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/main.css">
    <style>
        .wrapper_lazy_load {
            opacity: 0;
            transition: .3s;
            min-height: calc(100vh - 131px);
        }
    </style>
    <script src="/libs/jQuery/jquery-3-4-1.min.js"></script>
    <link rel='stylesheet' href='https://code.getmdl.io/1.3.0/material.indigo-pink.min.css'>
    <script defer src='https://code.getmdl.io/1.3.0/material.min.js'></script>
    <!-- Include pdfMake - http://pdfmake.org/ -->
    <script src='<?=BASEURL?>vendor/pdfMake/pdfmake.min.js'></script>
    <script src='<?=BASEURL?>vendor/pdfMake/vfs_fonts.js'></script>

    <!-- include THREEx.ArPatternFile -->
    <script src='<?=BASEURL?>vendor/threex-arpatternfile.js'></script>
    <script src="https://aframe.io/releases/0.6.1/aframe.min.js"></script>
    <script src="https://rawgit.com/donmccurdy/aframe-extras/master/dist/aframe-extras.loaders.min.js"></script>
    <script src="https://cdn.rawgit.com/jeromeetienne/AR.js/1.5.0/aframe/build/aframe-ar.js"> </script>

    <script>var BASEURL = "<?=BASEURL?>";</script>


    <style>
        .arjs-loader {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background-color: rgba(0, 0, 0, 0.8);
            z-index: 9999;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .arjs-loader div {
            text-align: center;
            font-size: 1.25em;
            color: white;
        }
    </style>
</head>
<body>

<? if($param[0]=='login' || $param[0]=='register') { return false; } else {?>
<header class="header">
    <div class="container">
        <div class="row align-items-center">
            <div class="shrink">
                <div class="logo">
                    <a href="<?= BASEURL ?>">
                        <svg id="Capa_1" enable-background="new 0 0 512 512" height="60" viewBox="0 0 512 512"
                             width="60" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="256" x2="256" y1="512"
                                            y2="0">
                                <stop offset="0" stop-color="#ffe59a"/>
                                <stop offset="1" stop-color="#ffffd5"/>
                            </linearGradient>
                            <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="256" x2="256" y1="407"
                                            y2="105">
                                <stop offset="0" stop-color="#ffde00"/>
                                <stop offset="1" stop-color="#fd5900"/>
                            </linearGradient>
                            <g>
                                <g>
                                    <circle cx="256" cy="256" fill="url(#SVGID_1_)" r="256"/>
                                </g>
                                <g>
                                    <g>
                                        <path d="m193.124 255.997 12.573 12.573 12.582-12.573-12.582-12.573zm50.302 50.302 12.573 12.573 12.573-12.573-12.573-12.582zm155.805-45.387c-.009 0-.009 0-.009 0l-4.915-4.915 4.923-4.915c10.359-10.385 10.359-27.266-.009-37.651-10.081-10.073-27.717-10.125-37.877.052l-4.741 4.811-12.626-12.626 4.923-4.915c10.359-10.385 10.359-27.266-.009-37.66-10.385-10.351-27.266-10.359-37.651.009l-4.915 4.915-12.625-12.626 4.863-4.785c10.394-10.394 10.443-27.381 0-37.842-10.385-10.351-27.266-10.359-37.651.009l-4.915 4.915-4.915-4.923c-10.385-10.351-27.266-10.359-37.651.009-10.382 10.382-10.473 27.37.052 37.877l4.811 4.741-12.625 12.626-4.915-4.923c-10.385-10.351-27.257-10.359-37.66.009-10.359 10.385-10.359 27.266.009 37.651l4.915 4.915-12.626 12.626-4.784-4.863c-10.064-10.073-27.743-10.073-37.842 0-10.359 10.385-10.359 27.266.009 37.651l4.915 4.915-4.923 4.915c-10.359 10.385-10.359 27.266.009 37.651 10.081 10.081 27.726 10.116 37.877-.052l4.741-4.811 12.626 12.626-4.923 4.915c-10.359 10.385-10.359 27.266.009 37.66 10.385 10.351 27.266 10.359 37.651-.009l4.915-4.915 12.625 12.626-4.863 4.785c-5.036 5.036-7.806 11.749-7.806 18.921 0 7.164 2.77 13.876 7.806 18.921 10.385 10.351 27.266 10.359 37.651-.009l4.915-4.915 4.915 4.923c10.401 10.367 27.265 10.36 37.651-.009 10.382-10.382 10.473-27.37-.052-37.877l-4.811-4.741 12.625-12.626 4.915 4.923c10.385 10.351 27.257 10.359 37.659-.009 10.359-10.385 10.359-27.266-.009-37.651l-4.915-4.915 12.626-12.626 4.784 4.863c10.073 10.064 27.743 10.081 37.842 0 10.36-10.386 10.36-27.266.001-37.651zm-174.665-61.505 25.147-25.147c3.473-3.473 9.1-3.473 12.573 0l25.147 25.147c3.473 3.473 3.473 9.1 0 12.573l-25.147 25.156c-1.667 1.667-3.925 2.605-6.287 2.605s-4.62-.938-6.287-2.605l-25.147-25.156c-3.472-3.472-3.472-9.099.001-12.573zm-12.582 88.024c-1.737 1.737-4.012 2.605-6.287 2.605s-4.55-.868-6.287-2.605l-25.147-25.147c-3.473-3.473-3.473-9.1 0-12.573l25.147-25.147c3.473-3.473 9.1-3.473 12.573 0l25.156 25.147c1.667 1.667 2.605 3.925 2.605 6.287s-.938 4.62-2.605 6.287zm75.449 25.155-25.147 25.147c-1.737 1.737-4.012 2.605-6.287 2.605s-4.55-.868-6.287-2.605l-25.147-25.147c-3.473-3.473-3.473-9.1 0-12.573l25.147-25.156c3.334-3.334 9.239-3.334 12.573 0l25.147 25.156c3.474 3.473 3.474 9.1.001 12.573zm50.302-50.302-25.147 25.147c-1.737 1.737-4.012 2.605-6.287 2.605s-4.55-.868-6.287-2.605l-25.156-25.147c-1.667-1.667-2.605-3.925-2.605-6.287s.938-4.62 2.605-6.287l25.156-25.147c3.473-3.473 9.1-3.473 12.573 0l25.147 25.147c3.475 3.473 3.475 9.1.001 12.574zm-69.162-56.59-12.573-12.573-12.573 12.573 12.573 12.582zm25.147 50.303 12.582 12.573 12.573-12.573-12.573-12.573z"
                                              fill="url(#SVGID_2_)"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="shrink ml-auto"><a href="<? BASEURL ?>apps/" class="btn btn-success">Create for FREE</a></div>
            <div class="shrink">
                <div class="user-info">
                    <div class="row align-items-center">
                        <? if ($this->logedin) { ?>
                            <div class="col text-right">
                                <div class="user-name"><?= $this->user->fio ?></div>
                                <div class="user-email"><?= $this->user->email ?></div>
                                <a href="<?= BASEURL ?>signout/">Signout</a>
                            </div>
                            <div class="shrink">
                                <div class="user-thumb">
                                    <img src="/images/user.png" alt="">
                                </div>
                            </div>
                        <? } else { ?>
                            <div class="shrink"><a href="<?= BASEURL ?>login/" class="btn btn-success">Signin</a></div>
                        <? } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header> <?}?>

    <div class="wrapper_lazy_load">