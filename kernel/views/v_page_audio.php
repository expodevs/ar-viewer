<html>
<head>
    <script src="https://aframe.io/releases/0.8.0/aframe.min.js"></script>
    <script src="https://jeromeetienne.github.io/AR.js/aframe/build/aframe-ar.js"></script>
    <!-- <script src="aframe-ar.js"></script> use this (v1.6) if this demo ever breaks-->
    <link rel="stylesheet" href="/css/main.css">
</head>
<body>
<a-scene embedded arjs='sourceType: webcam; debugUIEnabled: false;';>
    <div class="goal-block"></div>
    <a-assets>
        <audio id="sound" src="/data/<?=$this->app->object_url?>" preload="auto"></audio>
    </a-assets>
    <a-marker type="pattern" url="/data/markers/<?=$this->app->marker_url?>">
        <a-box position='0 0.5 0' material='opacity: 0' soundhandler>
        </a-box>
    </a-marker>
    <a-entity sound="src: #sound" autoplay="false"></a-entity>
    <a-entity camera></a-entity>
</a-scene>

<script>
    AFRAME.registerComponent('soundhandler', {
        tick: function () {
            var entity = document.querySelector('[sound]');
            if (document.querySelector('a-marker').object3D.visible == true) {
                entity.components.sound.playSound();
            } else {
                entity.components.sound.pauseSound();
            }

        }
    });
</script>

<style>
    .goal-block {
        position: absolute;
        top: calc(50% - 100px);
        left: calc(50% - 100px);
        border: 1px solid red;
        border-radius: 10px;
        width: 200px;
        height: 200px;
    }
</style>

</body>
</html>