<!doctype HTML>
<html>
<script src="/js/aframe.min.js"></script>
<script src="/js/aframe-ar.js"></script>
<body style="margin: 0px; overflow: hidden;">

<a-scene embedded vr-mode-ui="enabled: false;" arjs="debugUIEnabled: false;">


    <a-marker type="pattern" url="/data/markers/<?=$this->app->marker_url?>">
        <a-image width="2" height="2" rotation="270 0 0" roughness="1" src="/data/<?=$this->app->object_url?>"></a-image>
    </a-marker>

    <a-entity camera></a-entity>

</a-scene>
</body>
</html>