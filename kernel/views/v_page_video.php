<html>
<head>
    <script src="https://aframe.io/releases/0.8.0/aframe.min.js"></script>
    <script src="https://jeromeetienne.github.io/AR.js/aframe/build/aframe-ar.js"></script>
    <!-- <script src="aframe-ar.js"></script> use this (v1.6) if this demo ever breaks-->
    <link rel="stylesheet" href="/css/main.css">
    <style>
        .btns_control {
            position: absolute;
            bottom: 20px;
            left: 0;
            width: 100%;
            font-size: 18px;
            z-index: 9;
        }
        .btns_control a {
            width: 100%;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container btns_control">
    <div class="row">
        <div class="col_6">
            <a id="playButton" onclick="play('alpha')" class="btn btn_default">Play</a>
        </div>
        <div class="col_6">
            <a id="pauseButton" onclick="pause('alpha')" class="btn btn_default">Pause</a>
        </div>
    </div>
</div>

<a-scene embedded vr-mode-ui="enabled: false;" arjs="debugUIEnabled: false;">
    <a-assets><video id="alpha" width="300" height="150" autoplay loop="true" src="/data/<?=$this->app->object_url?>"></video></a-assets>
    <a-marker-camera type="pattern" url="/data/markers/<?=$this->app->marker_url?>">
        <a-video id="vid" width='2.5' height='1.5' rotation="270 0 0" src="#alpha"></a-video>
    </a-marker-camera>
</a-scene>

<script>

    function play(id){
        var aVideoAsset = document.querySelector('#' + id);
        aVideoAsset.play().catch(function (error) {
            aVideoAsset.pause();
        });
        aVideoAsset.setAttribute('loop', 'false');
    }

    function pause(id) {
        var aVideoAsset = document.querySelector('#' + id);
        aVideoAsset.pause().catch(function (error) {
            aVideoAsset.play();
        });
        aVideoAsset.setAttribute('loop', 'false');
    }
</script>

</body>
</html>