$(document).ready(function() {
    $('.list-apps-wrapper .item-app').click(function() {
        $('.list-apps-wrapper .item-app').not($(this)).removeClass('active');
        $(this).toggleClass('active');
        if($('.list-apps-wrapper .item-app').hasClass('active')) {
            $('.list-apps-wrapper').addClass('active');
        } else {
            $('.list-apps-wrapper').removeClass('active');
        }
        
    });


    $(".btn-create-project-fixed").click(function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
			top = $(id).offset().top;
		
		$('body,html').animate({scrollTop: top}, 500);
	});


    $(document).scroll(function () { // remove "$"
        var y = $(this).scrollTop();        
        if (y > 100) {
            $('.btn-create-project').fadeIn();
        } else {
            $('.btn-create-project').fadeOut();
        }
    });



    $('#fileObject').change(function(){
        var file = this.files[0];
        console.log(file);
        objectName = file.name;
        $('[name="obj_title_app"]').val(objectName);
        $('.first-window-layout-wrap .tile .lay.file').text(objectName);
    });

    var innerImageURL = null;
    var fullMarkerURL = null;
    var imageName = null;

    innerImageURL = '/images/inner-arjs.png';


    $('#fileMarker').change(function(){
        console.log(this);
        var file = this.files[0];
        
        imageName = file.name;
        // remove file extension
        console.log(imageName);
        imageName = imageName.substring(0, imageName.lastIndexOf('.')) || imageName;

        // debugger

        var reader = new FileReader();
        reader.onload = function(event){
            innerImageURL = event.target.result;
            updateFullMarkerImage();
        };
        reader.readAsDataURL(file);

        setTimeout(() => {
            THREEx.ArPatternFile.encodeImageURL(innerImageURL, function onComplete(patternFileString){
            
                $('[name="marker_app"]').val(patternFileString);
                $('[name="marker_img_app"]').val(fullMarkerURL.replace('data:image/png;base64,',''));
            })
        }, 500);
        
        console.log();
    });

    function updateFullMarkerImage(){
        // get patternRatio

        THREEx.ArPatternFile.buildFullMarker(innerImageURL, 0.6, 512, 'black', function onComplete(markerUrl){
            fullMarkerURL = markerUrl;

            var fullMarkerImage = document.createElement('img');
            fullMarkerImage.src = fullMarkerURL;

            // put fullMarkerImage into #imageContainer
            var container = document.querySelector('#imageContainer');
            while (container.firstChild) container.removeChild(container.firstChild);
            container.appendChild(fullMarkerImage)
        })
    }


    $('.login-wrap .input-wrap').on('click','.btn-view-pass.noactive',function() {
        $(this).parent().find('input').attr('type','text');
        $(this).removeClass('noactive');
        $(this).addClass('active');
        $(this).html('<svg width="24" height="24" viewBox="0 0 20 14" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M10 9C11.1046 9 12 8.10457 12 7C12 5.89543 11.1046 5 10 5C8.89543 5 8 5.89543 8 7C8 8.10457 8.89543 9 10 9ZM10 11C12.2091 11 14 9.20914 14 7C14 4.79086 12.2091 3 10 3C7.79086 3 6 4.79086 6 7C6 9.20914 7.79086 11 10 11Z" fill="#1F4CED"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M5.65914 3.63233C4.34002 4.58138 3.26409 5.74418 2.58386 6.57005C2.48409 6.69118 2.40958 6.78179 2.34785 6.86149C2.29899 6.92459 2.26786 6.96852 2.24756 7C2.26786 7.03148 2.29899 7.07541 2.34785 7.13851C2.40958 7.21821 2.48409 7.30882 2.58386 7.42995C3.26409 8.25582 4.34002 9.41862 5.65914 10.3677C6.98616 11.3224 8.47421 12 10 12C11.5258 12 13.0139 11.3224 14.3409 10.3677C15.66 9.41862 16.7359 8.25582 17.4162 7.42995C17.5159 7.30882 17.5904 7.21821 17.6522 7.13851C17.701 7.07541 17.7322 7.03147 17.7525 7C17.7322 6.96852 17.701 6.92458 17.6522 6.86149C17.5904 6.78179 17.5159 6.69118 17.4162 6.57005C16.7359 5.74418 15.66 4.58138 14.3409 3.63233C13.0139 2.6776 11.5258 2 10 2C8.47421 2 6.98616 2.6776 5.65914 3.63233ZM4.49112 2.00884C5.99416 0.927471 7.89008 0 10 0C12.1099 0 14.0059 0.927471 15.5089 2.00885C17.0198 3.0959 18.2201 4.40025 18.9599 5.29853C18.9835 5.32717 19.0081 5.35662 19.0334 5.38694C19.3468 5.76236 19.7703 6.26957 19.7703 7C19.7703 7.73043 19.3468 8.23764 19.0334 8.61306C19.0081 8.64338 18.9835 8.67283 18.9599 8.70148C18.2201 9.59976 17.0198 10.9041 15.5089 11.9912C14.0059 13.0725 12.1099 14 10 14C7.89008 14 5.99416 13.0725 4.49111 11.9912C2.98018 10.9041 1.77996 9.59975 1.04009 8.70147C1.0165 8.67283 0.991912 8.64338 0.966604 8.61306C0.65318 8.23764 0.229736 7.73043 0.229736 7C0.229736 6.26957 0.653181 5.76236 0.966605 5.38694C0.991913 5.35662 1.0165 5.32717 1.04009 5.29852C1.77996 4.40024 2.98018 3.0959 4.49112 2.00884Z" fill="#1F4CED"/>\n' +
            '</svg>');
    });
    $('.login-wrap .input-wrap').on('click','.btn-view-pass.active',function () {
        $(this).parent().find('input').attr('type', 'password');
        $(this).removeClass('active');
        $(this).addClass('noactive');
        $(this).html('<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M15.9202 12.7988C15.9725 12.5407 16 12.2736 16 12C16 9.79086 14.2091 8 12 8C11.7264 8 11.4593 8.02746 11.2012 8.07977L15.9202 12.7988ZM8.66676 9.78799C8.24547 10.4216 8 11.1821 8 12C8 14.2091 9.79086 16 12 16C12.8179 16 13.5784 15.7545 14.212 15.3332L12.7381 13.8594C12.5098 13.9501 12.2607 14 12 14C10.8954 14 10 13.1046 10 12C10 11.7393 10.0499 11.4902 10.1406 11.2619L8.66676 9.78799Z" fill="#B1C0DD"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M16.5191 17.6405L15.0499 16.1712C14.0776 16.6805 13.0477 17 12 17C10.4742 17 8.98616 16.3224 7.65914 15.3677C6.34002 14.4186 5.26409 13.2558 4.58386 12.43C4.48409 12.3088 4.40958 12.2182 4.34785 12.1385C4.29899 12.0754 4.26786 12.0315 4.24756 12C4.26786 11.9685 4.29899 11.9246 4.34785 11.8615C4.40958 11.7818 4.48409 11.6912 4.58386 11.57C5.24928 10.7622 6.29335 9.63187 7.57331 8.69463L6.14458 7.2659C4.79643 8.29616 3.72247 9.47005 3.04009 10.2985C3.01651 10.3272 2.99192 10.3566 2.96662 10.3869L2.96661 10.3869C2.65318 10.7624 2.22974 11.2696 2.22974 12C2.22974 12.7304 2.65318 13.2376 2.9666 13.6131L2.96682 13.6133C2.99205 13.6435 3.01657 13.6729 3.04009 13.7015C3.77996 14.5998 4.98018 15.9041 6.49111 16.9912C7.99416 18.0725 9.89008 19 12 19C13.67 19 15.206 18.419 16.5191 17.6405ZM8.80682 5.6855C9.79062 5.26871 10.8643 5 12 5C14.1099 5 16.0059 5.92747 17.5089 7.00885C19.0198 8.0959 20.2201 9.40025 20.9599 10.2985C20.9835 10.3272 21.0081 10.3566 21.0334 10.3869L21.0334 10.3869C21.3468 10.7624 21.7703 11.2696 21.7703 12C21.7703 12.7304 21.3468 13.2376 21.0334 13.6131C21.0081 13.6434 20.9835 13.6728 20.9599 13.7015C20.4733 14.2923 19.7874 15.0589 18.945 15.8237L17.529 14.4077C18.3089 13.708 18.9539 12.9912 19.4162 12.43C19.5159 12.3088 19.5904 12.2182 19.6522 12.1385C19.701 12.0754 19.7322 12.0315 19.7525 12C19.7322 11.9685 19.701 11.9246 19.6522 11.8615C19.5904 11.7818 19.5159 11.6912 19.4162 11.57C18.7359 10.7442 17.66 9.58138 16.3409 8.63233C15.0139 7.6776 13.5258 7 12 7C11.449 7 10.9029 7.08837 10.3676 7.24624L8.80682 5.6855Z" fill="#B1C0DD"/>\n' +
            '<path d="M5 2L21 18" stroke="#B1C0DD" stroke-width="2"/>\n' +
            '</svg>');
    });
});


let bg = document.querySelector('.man');
window.addEventListener('mousemove', function(e) {
    let x = e.clientX / window.innerWidth;
    let y = e.clientY / window.innerHeight;
    bg.style.transform = 'translate(-' + x * 10 + 'px, -' + y * 10 + 'px)';
});