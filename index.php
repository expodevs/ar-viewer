<?
session_start();

if(isset($_SERVER['REQUEST_URI']) && preg_match('~[а-яёА-ЯЁ\%]~', $_SERVER['REQUEST_URI'])){  header("HTTP/1.0 404 Not Found");  exit;  }

//////////////////////////////////////////////////////////////////////////////
//ПОЛУЧЕНИЕ ПЕРЕМЕННОЙ $param - РЕЗУЛЬТАТ РАЗЛОЖЕНИЯ URL В МАССИВ
$param = [];
$REQUEST_URI = explode("?", $_SERVER['REQUEST_URI'])[0];
if(empty($REQUEST_URI) || $REQUEST_URI=="/"){  }
else{ $param = explode("/", $REQUEST_URI); }
$param = array_diff($param, array('', ' ', null));
$param = array_values($param);


//////////////////////////////////////////////////////////////////////////////
//ПРОВЕРКА ЮЗЕР-АГЕНТА + ОПРЕДЕЛЕНИЕ МОБИЛЬНОСТИ
$useragent = $_SERVER['HTTP_USER_AGENT'];
$mobile = false;
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iPhone|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent,0,4))){
    $mobile = true;
}
if(strpos($useragent, "Firefox")!==false){  $image_type = "webp";  }
elseif(strpos($useragent, "Opera")!==false){  $image_type = "webp";  }
elseif(strpos($useragent, "Chrome")!==false){  $image_type = "webp";  }
elseif(strpos($useragent, "MSIE")!==false){  $image_type = "IE TODO";  }
elseif(strpos($useragent, "Trident")!==false){  $image_type = "IE TODO";  }
elseif(strpos($useragent, "Safari")!==false){  $image_type = "IOS TODO";  }
else{  $image_type = "UNDEF TODO";  }


//////////////////////////////////////////////////////////////////////////////
define("BASEPATH", true);
require_once 'kernel/configs/config.php';


//////////////////////////////////////////////////////////////////////////////
//БИБЛИОТЕКИ
function include_library($library_name){
	include_once("kernel/libraries/l_".$library_name.".php");
}

//////////////////////////////////////////////////////////////////////////////
//ХЕЛПЕРЫ
$entries = scandir("kernel/helpers");
foreach($entries as $entry) {
    if(strpos($entry, "h_") === 0){
		include_once("kernel/helpers/".$entry);
    }
}

//////////////////////////////////////////////////////////////////////////////
//ВИДЫ
class view extends kernel{
	function __construct($data){
        //Создаем те-же свойства что и у источника kernel
        foreach($data as $key=>$opt){  $this->$key=$opt;  }
	}
	protected function load($view){
        global $param;
        $pathArr = explode("/", $view);
        ob_start("html_minifier");
        if(count($pathArr)==1){
			include_once("kernel/views/v_$view.php");
        }elseif(count($pathArr)==2){
			include_once("kernel/views/".$pathArr[0]."/v_".$pathArr[1].".php");
        }elseif(count($pathArr)==3){
			include_once("kernel/views/".$pathArr[0]."/".$pathArr[1]."/v_".$pathArr[2].".php");
        }
        ob_end_flush();
	}
}
function html_minifier($buffer){
    preg_match_all('~(?<!:|\')//[^\r\n]*(?=\r\n)~', $buffer, $matches);
    if(isset($matches[0]) && !empty($matches[0])){
        $buffer = str_replace($matches[0], '', $buffer);
    }
    
    $search = array(
        '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
        '/[^\S ]+\</s',     // strip whitespaces before tags, except space
        '/(\s)+/s',         // shorten multiple whitespace sequences
        '/<!--(.|\s)*?-->/' // Remove HTML comments
    );
    $replace = array('>', '<', '\\1', '');
    return preg_replace($search, $replace, $buffer);
}

//////////////////////////////////////////////////////////////////////////////
//КОНТРОЛЕРЫ
//если нет никаких параметров, т.е. только домен -- то это главная страница
if(empty($param)){
	if(!file_exists("kernel/controlers/c_index.php")){get_error(); exit;}
	include_once("kernel/controlers/c_index.php");
	
	$class = new index(array());
	if(method_exists('index', 'index')){
		$class->index();
	}else{get_error(); exit;}
}
//есть первый параметр -- ищем такой контроллер, если ненаходим -- ищем такой метод в индексе
else{
    if($param[0]=='index'){header("HTTP/1.1 301 Moved Permanently"); header("Location: ".BASEURL); exit;} //Если первый параметр index - редиректим на главную
	$controller_found = false;
    
    ////////////////////////////ПОДМЕНА С НИЖНЕГО ПОДЧЕРКИВАНИЯ НА ТИРЕШКУ////////////////////////
    #if($param[0]=="o-kompanii"){ $param[0]="o_kompanii"; }
    #elseif($param[0]=="uslugi-avtoservisa"){ $param[0]="uslugi_avtoservisa"; }
    #elseif($param[0]=="corporative-clients"){ $param[0]="corporative_clients"; }
    ////////////////////////////ПОДМЕНА С НИЖНЕГО ПОДЧЕРКИВАНИЯ НА ТИРЕШКУ////////////////////////
	
	if(file_exists("kernel/controlers/c_$param[0].php")){
		$controller_found = true;
		include_once("kernel/controlers/c_$param[0].php");
	}
	
	if($controller_found){
		//если нет вызова метода в контроллере -- подключаем индекс в контроллере
		if(empty($param[1])){
			if(! method_exists($param[0], 'index')){get_error(); exit;}
			
			$controller = new $param[0]($param);
			$controller->index();
            
        }elseif(method_exists($param[0], $param[1])){
            $controller = new $param[0]($param);
			$controller->{$param[1]}($param);
			
        //если вызывается динамический метод в контроллере который генерирует страницы из $param
        }elseif(method_exists($param[0], 'dynamic_method')){
            $controller = new $param[0]($param);
			$controller->dynamic_method($param);
            
		//иначе вызывается какой-то метод в контроллере
		}else{
			if(! method_exists($param[0], $param[1])){get_error(); exit;}
			
			//проверяем не передаем ли мы в метод контроллера параметр когда он его не просит
			if(isset($param[2])){
				$r  = new \ReflectionMethod($param[0], $param[1]);
				$numberOfParams = $r->getNumberOfRequiredParameters();
				if($numberOfParams == 0){get_error(); exit;}
			}
			$controller = new $param[0]($param);
			$controller->{$param[1]}($param);
		}
	}
	
	if(! $controller_found){
		if(!file_exists("kernel/controlers/c_index.php")){get_error(); exit;}
		$controller_found = true;
		include_once("kernel/controlers/c_index.php");
		
        //Сначала подключаем контроллер в индексе так как там могет быть некое переопределение логики той что следует ниже этого вызова.
        $class = new index($param);
        
		if(! method_exists('index', $param[0])){ $method = 'dynamic_method'; /*get_error(); exit;*/ }else{ $method = $param[0]; }
		
		//проверяем не передаем ли мы в метод параметр когда он его не просит
		$r  = new \ReflectionMethod('index', $method);
		$numberOfParams = $r->getNumberOfRequiredParameters();
        
		if(isset($param[1])){
			//Если передается параметр но метод его не просит = Ошибка
			if($numberOfParams == 0){get_error(); exit;}
			$class->$method($param);
			
		}else{
			//Если не передается параметр но метод его просит -- то передадим параметр
			if($numberOfParams > 0){
				$class->$method($param);
			}else{
				$class->$method();
			}
		}
		
	}
	
	if(! $controller_found){get_error(); exit;}
}

//////////////////////////////////////////////////////////////////////////////
//МОДЕЛИ
function include_model($modelname){
	include_once("kernel/models/$modelname/index.php");
}



//////////////////////////////////////////////////////////////////////////////
//KERNEL

class kernel{
	
	protected $options;
	
	function __construct(){
		global $useragent;
        global $mobile;
        global $image_type;
		
        $this->library_singleton('crud');
		//$this->library_singleton('memorycache');
        
        //if(!empty($_POST)){ $this->check_for_xss_attacks_post($_POST); }
        if(!empty($_GET)){ $this->check_for_xss_attacks_get($_GET); }
        if(!empty($_COOKIE)){ $this->check_for_xss_attacks_cookie($_COOKIE); }
        
		$this->options = new stdClass();
        $this->options->user = NULL;
        $this->options->logedin = false;
        $this->options->logedinadmin = false;
        $this->check_login_cookies();
        $this->options->mobile = $mobile;
        $this->options->image_type = $image_type;
        
        //ДЛЯ ССЫЛОК НА ДИНАМИЧЕСКИЕ СТРАНИЦЫ В ШАПКЕ САЙТА

        //ДЛЯ СЕОШКИ
        $this->options->seo = new stdClass();
        $this->options->seo->meta_title = "";
        $this->options->seo->meta_keywords = "";
        $this->options->seo->meta_description = "";

	}
    
    protected function check_for_xss_attacks_post(&$post){
        
        //На некоторых страницах отменяем защиту
        global $param;
        if(!empty($param) && ($param[0]=="admin")){ return; }
        
        $filter = array("<", ">", "(", ")", "'");
        foreach($post as $key => $p){ $post[$key] = str_replace ($filter, "", $p); }
        
        //Проверка по стоп словам
        $needStop = false;
        foreach($post as $p){
            foreach(stop_words() as $stopWord){
                if(stripos($p, $stopWord) !== false){ $needStop = true; break 2; }
            }
        }
        
//        //Проверка правильности имени
//        if($needStop==false){ if(isset($_POST['name']) && !empty($_POST['name'])){ if(stop_abnormal_name($_POST['name'])){ $needStop = true; } } }
//        
//        //Проверка правильности телефона
//        if($needStop==false){ if(isset($_POST['phone']) && !empty($_POST['phone'])){ if(stop_abnormal_phone($_POST['phone'])){ $needStop = true; } } }
        
        if($needStop){
            header('HTTP/1.0 423 Reason Blocked by stop word');
            echo "Hi spammer!!! Your IP-address will be added to all black lists automatically";
            
            $client  = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            /*                                TODO пока не будем блокировать IP с занесением в БД. Может потом понадобится.
            $ip = "";
            if(filter_var($client, FILTER_VALIDATE_IP)){ $ip = $client; }
            elseif(filter_var($forward, FILTER_VALIDATE_IP)){ $ip = $forward; }
            if(!empty($ip)){ $this->crud->create('r_blocked_ip', array('ip'=>$ip, 'message'=>$p)); }
            */
            exit;
        }
        
    }
    
    protected function check_for_xss_attacks_get(&$get){
        $filter = array("<", ">", "(", ")", "'");
        foreach($get as $key => $p){
            $get[$key] = str_replace ($filter, "|", $p);
        }
    }
    
    protected function check_for_xss_attacks_cookie(&$cookie){
        $filter = array("<", ">", "(", ")", "'");
        foreach($cookie as $key => $p){
            $cookie[$key] = str_replace ($filter, "|", $p);
        }
    }
    
    
	
	protected function library($library){
		include_library($library);
		$this->$library = new $library;
	}
	
	protected function library_singleton($library){
		include_library($library);
		$this->$library = $library::getInstance();
	}
	
	protected function view($viev){
		$this->$viev = new view($this->options);
		$this->$viev->load($viev);
	}
    
    protected function model($modelname){
        include_model($modelname);
        $this->$modelname = new $modelname;
    }
    
    private function check_login_cookies(){
        
        //Проверка залогинености имейлом и паролем
        if(isset($_COOKIE['email']) && isset($_COOKIE['password']) && !empty($_COOKIE['email']) && !empty($_COOKIE['password'])){
            $this->options->user = $this->crud->read('b_users', array('email'=>$_COOKIE['email'], 'password'=>$_COOKIE['password']));
            if(gettype($this->options->user)=="integer"){
                $this->options->user = NULL;
            }else{
                $this->options->logedin = true;
                if($this->options->user->admin){ $this->options->logedinadmin = true; }
            }
        }
    }
    
}